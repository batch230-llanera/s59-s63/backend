const mongoose = require("mongoose");

const gallerySchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Name is required"]
	},
	kind: {
		type: String,
		required: [true, "Type of cake is required"]
	},
	image: {
		type: String,
		required: [true, "Photo is required"]
	},
	isActive: {
		type: Boolean,
		default: true
	}
})

module.exports = mongoose.model("Gallery", gallerySchema)
