const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	orderStatus: {
		type: String,
		default: "order placed"
	},
	totalAmount: {
		type: Number,
		default: 0
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	},
	userInfo: [
		{
			fullName: {
			type: String,
			required: [true, "User ID is required"]
			},
			address: {
				type: String,
				required: [true, "Last name is required"]
			},
			mobileNumber: {
				type: String,
				required: [true, "Mobile number is required"]
			}
		}

	],
	products : [
		{
			name: {
				type: String,
				required: [true, "Product name is required"]
			},
			_id: {
				type: String,
				required: [true, "Product Id is required"]
			},
			price: {
				type: Number,
				required: [true, "Price is required"]
			},
			quantity: {
				type: Number,
				default: 0
			},
			subTotal: {
				type: Number,
			default: 0
			}

		}
	]
})

module.exports = mongoose.model("Order", orderSchema)

