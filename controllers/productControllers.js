const User = require("../models/users.js");
const Product = require("../models/products.js");
const Order = require("../models/orders.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");
const mongoose = require("mongoose");


module.exports.addProduct = (request, response) => {

	const checkAccess = auth.decode(request.headers.authorization).isAdmin;

	if(checkAccess == true){

		let newProduct = new Product({
		name : request.body.name,
		productImage: request.body.productImage,
		description : request.body.description,
		price : request.body.price,
		orderSlotsAvailable : request.body.orderSlotsAvailable
		})

		return newProduct.save()
		.then(product => {
			console.log(product);
			response.send(true);
		})
		.catch(error => {
			console.log(error);
			response.send(false);
		})
	}
	else {
		return response.status(402).send("Admin access required!");
	}

}


module.exports.getAllProducts = (request, response) => {
	return Product.find({})
	.then(result => response.send(result));
}


module.exports.getActiveProducts = (request, response) => {
	return Product.find({isActive: true})
	.then(result => response.send(result));
}


module.exports.getProduct = (request, response) => {
	console.log(request.params.productId);

	return Product.findById(request.params.productId)
	.then(result => {
		console.log(result)
		return response.send(result)	
	});
}

/*
module.exports.getProductv2 = (req, res) =>{
	console.log(req.params._id);
	console.log("test")
	return Product.findOne({_id: req.params._id}).then(result => {
		console.log(result)
		return res.send(result)});
}*/


module.exports.updateProduct = (request, response) =>{
	const checkAccess = auth.decode(request.headers.authorization).isAdmin;

	if(checkAccess == true){
		let updateProduct = {
			name: request.body.name,
			productImage: request.body.productImage,
			description: request.body.description,
			price: request.body.price,
			orderSlotsAvailable: request.body.orderSlotsAvailable
		}

		return Product.findByIdAndUpdate(request.params._id, updateProduct, {new:true})
		.then(result =>{
			console.log(result);
			response.send(result);
		})
		.catch(error =>{
			console.log(error);
			response.send(false);
		});
	}
	else{
		return response.status(402).send("Admin access required!");
	}
}


module.exports.archiveProduct = (request, response) => {

	const checkAccess = auth.decode(request.headers.authorization).isAdmin;

	let updateIsActiveField = {
		isActive: request.body.isActive
	}

	if(checkAccess == true){
		return Product.findByIdAndUpdate(request.params._id, updateIsActiveField)
		.then(result => {
			console.log(result);
			response.send(true);
		})
		.catch(error =>{
			console.log(error);
			response.send(false);
		})
	}
	else{
		return response.status(402).send("Admin access required!");
	}
}














