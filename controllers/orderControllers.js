const User = require("../models/users.js");
const Product = require("../models/products.js");
const Order = require("../models/orders.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

module.exports.orderPlaced = async (request, response) => {
	const checkAccess = auth.decode(request.headers.authorization).isAdmin;

	if(checkAccess == true){
		return response.status(402).send("Logged in as Admin. Please login as a User to order.");
	}
	else{
		const userData = auth.decode(request.headers.authorization);

		let name = await Product.findById(request.body._id).then(product => product.name);
		let price =  await Product.findById(request.body._id).then(product => product.price)
		let orderSlotsAvailable = await Product.findById(request.body._id).then(product => product.orderSlotsAvailable)
		let address = await User.findById(userData.id).then(userInfo => userInfo.address)
		let mobileNumber = await User.findById(userData.id).then(userInfo => userInfo.mobileNumber)
		let fullName = await User.findById(userData.id).then(userInfo => userInfo.fullName)

		let _id = request.body._id
		let userId = userData.id
		let	email = userData.email
		let quantity = []

		let user = await User.exists({_id: userData.id});
		console.log(user)

		if (userId == null || user == null) {
    		return response.status(400).send({ status: false, message: "Invalid user ID" })};

    	if (_id == null) {
    		return response.status(400).send({ status: false, message: "Invalid product" })};


    	let updateOrderSlostsAvailable = await Product.findByIdAndUpdate(_id).then(product => {
    		(product.orderSlotsAvailable -= 1) -1
    		return product.save().then(result => {
    			console.log(result);
    			return true
    		})
    		.catch(error => {
    			console.log(error)
    			return false
    		})
    		console.log(updateOrderSlostsAvailable)
    		return response.send(updateOrderSlostsAvailable)
    	})


    	let cart = await Order.findOne({ _id:_id})

    	if (cart) {
    	    let itemIndex = cart.products.findIndex((p) => p._id == _id);

    	    if (itemIndex > -1) {
    	      let productItem = cart.products[itemIndex];
    	      productItem.quantity += 1;
    	      productItem.orderSlotsAvailable -= 1;
    	      productItem.subTotal = productItem.price * productItem.quantity;
    	      cart.products[itemIndex] = productItem;

    	      let totalAmount = cart.totalAmount + productItem.price
    	      cart.totalAmount = totalAmount
    	    } 
    	    else {
    	      let totalAmount = cart.totalAmount + price
    	      cart.totalAmount = totalAmount

    	      let subTotal = price
    	      orderSlotsAvailable -= 1

    	      cart.products.push({ name: name, _id: _id, quantity: 1, orderSlotsAvailable: orderSlotsAvailable, price: price, subTotal : subTotal});
    	    }
    	    cart = await cart.save();
    	    return response.status(200).send({ status: true, Cart: cart });

    	    let updateOrderSlostsAvailable = await Product.findByIdAndUpdate(_id).then(product => {
    	    	product.orderSlotsAvailable -=1
    	    	return product.save().then(result => {
    	    		console.log(result);
    	    		return true
    	    	})
    	    	.catch(error => {
    	    		console.log(error)
    	    		return false
    	    	})
    	    	console.log(updateOrderSlostsAvailable)
    	    	return response.send(updateOrderSlostsAvailable)
    	    })
    	  } 
    	  else {
    	  	let totalAmount = price
    	  	let subTotal = price

    	  	orderSlotsAvailable -= 1

    	    const newCart = await Order.create({
    	      totalAmount,
    	      userInfo: [{ fullName: fullName, address: address, mobileNumber: mobileNumber}],
    	      products: [{ name: name, _id: _id, orderSlotsAvailable: orderSlotsAvailable, price: price, quantity: 1, subTotal: subTotal}],
    	    });

    	    return response.status(201).send({ status: true, Cart: newCart });

    
    	  }
    	
	}
}



module.exports.decreaseQuantity = async (request, response) => {
	const checkAccess = auth.decode(request.headers.authorization).isAdmin;

	if(checkAccess == true){
		return response.status(402).send("Logged in as Admin. Please login as a User to edit order.");
	}
	else{
		const userData = auth.decode(request.headers.authorization);

		let _id = request.body._id

		let userId = userData.id
		let	email = userData.email

		let user = await User.exists({_id: userData.id});
		console.log(user)

		if (userId == null || user == null) {
    		return response.status(400).send({ status: false, message: "Please login to order." })};

    	let updateOrderSlostsAvailable = await Product.findByIdAndUpdate(_id).then(product => {
    		(product.orderSlotsAvailable += 1) +1
    		return product.save().then(result => {
    			console.log(result);
    			return true
    		})
    		.catch(error => {
    			console.log(error)
    			return false
    		})
    		console.log(updateOrderSlostsAvailable)
    		return response.send(updateOrderSlostsAvailable)
    	})

    	let cart = await Order.findOne({ _id: _id})

    	if (cart == null){
    		return response.status(400).send({ status: false, message: "Cart is empty" })
    	}
    	else {
    		let itemIndex = cart.products.findIndex((p) => p._id == _id);

    		if (itemIndex > -1) {
    		let productItem = cart.products[itemIndex];

    		productItem.quantity -= 1; 		
    		productItem.orderSlotsAvailable += 1;
    	    productItem.subTotal = productItem.price * productItem.quantity;
    		cart.products[itemIndex] = productItem;
    		let totalAmount = cart.totalAmount - productItem.price
    	    cart.totalAmount = totalAmount

    	    if(productItem.quantity <= 0){cart.products.splice(itemIndex, 1);}
    		if(cart.totalAmount <= 0){(itemIndex, 1)}

    		cart = await cart.save();
    		return response.status(200).send({ status: true, Cart: cart });
  			}
  			response.status(400).send({ status: false, message: "Item does not exist in cart" });

    	}  	

	}
}

module.exports.viewCart = async (request, response) => {
	const checkAccess = auth.decode(request.headers.authorization).isAdmin;

	if(checkAccess == true){
		return response.status(402).send("Logged in as Admin. Please login as a User to view cart.");
	}
	else{
		const userData = auth.decode(request.headers.authorization);

		let userId = userData.id
		let	email = userData.email

		let user = await User.exists({_id: userData.id});
		console.log(user)

		if (userId == null || user == null) {
		    return response.status(400).send({ status: false, message: "Please login to view cart." })};

		let cart = await Order.findOne({ _id: _id})

		if (cart == null){
		    return response.status(400).send({ status: false, message: "Cart is empty" })
		}
		else{
			console.log(cart)
			response.send(cart)
		}

	}

}

module.exports.emptyCart = async (request, response) => {
	const checkAccess = auth.decode(request.headers.authorization).isAdmin;

	if(checkAccess == true){
		return response.status(402).send("Logged in as Admin. Please login as a User to empty cart.");
	}
	else{
		const userData = auth.decode(request.headers.authorization);

		let userId = userData.id
		let	email = userData.email

		let user = await User.exists({_id: userData.id});
		console.log(user)

		if (userId == null || user == null) {
		    return response.status(400).send({ status: false, message: "Please login to remove items in cart." })};

		let cart = await Order.findOne({ _id: _id})

		if (cart == null){
		    return response.status(400).send({ status: false, message: "Cart is empty" })
		}
		else{
			Order.findByIdAndRemove(request.params._id)
			.then(result => response.send(result))
			.catch(error => response.send(error));

		}

	}

}


module.exports.checkOut = async (request, response) => {
	const checkAccess = auth.decode(request.headers.authorization).isAdmin;

	if(checkAccess == true){
		return response.status(402).send("Logged in as Admin. Please login as a User to complete the order.");
	}
	else{
		const userData = auth.decode(request.headers.authorization);

		let userId = userData.id
		let	email = userData.email

		let user = await User.exists({_id: userData.id});
		console.log(user)

		if (userId == null || user == null) {
		    return response.status(400).send({ status: false, message: "Please login to order." })};

		let cart = await Order.findOne({ _id: _id})

		if (cart == null){
		    return response.status(400).send({ status: false, message: "Cart is empty." })
		}
		else{
			Order.findByIdAndUpdate(request.params._id)

			cart.orderStatus = "order placed"	


			cart = await cart.save();
    		return response.status(200).send({ status: "completed", Cart: cart });
		}
		response.status(400).send({ status: false, message: "Order not placed" });

	    let updatedProduct = await Product.findByIdAndUpdate(products._id).then(product => {
		products.orderSlotsAvailable -= products.quantity;

		product.orders.push({
			orderIds: request.params._id
		})

		return product.save()
			.then(result => {
			console.log(result);
			return true
			})
			.catch(error => {
			console.log(error);
			return false
			})
			})

			console.log(updatedProduct)
			response.send(updatedProduct)
				
	}

}



module.exports.getAllOrders = (request, response) => {

	const checkAccess = auth.decode(request.headers.authorization).isAdmin;

	if(checkAccess == true){
		return Order.find({})
		.then(result => response.send(result))
		.catch(error => response.send(error));
	}
	else{
		return response.status(402).send("Please login as a Admin to view orders.")
	}
}

module.exports.getOrder = (request, response) => {
	console.log(request.params._id);

	return Order.findById(request.params._id)
	.then(result => response.send(result));
}




