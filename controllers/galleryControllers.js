const User = require("../models/users.js");
const Gallery = require("../models/gallery.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");
const mongoose = require("mongoose");


module.exports.addPhoto = (request, response) => {

	const checkAccess = auth.decode(request.headers.authorization).isAdmin;

	if(checkAccess == true){

		let newGallery = new Gallery({
		name : request.body.name,
		kind : request.body.kind,
		image : request.body.image
		})

		return newGallery.save()
		.then(result => {
			console.log(result);
			response.send(true);
		})
		.catch(error => {
			console.log(error);
			response.send(false);
		})
	}
	else {
		return response.status(402).send("Admin access required!");
	}

}


module.exports.getAllPhotos = (request, response) => {
	return Gallery.find({})
	.then(result => response.send(result));
}


module.exports.getMessagePhotos = (request, response) => {
	return Gallery.find({isActive: true, kind:"Message Cake"})
	.then(result => response.send(result));
}

module.exports.getWeddingPhotos = (request, response) => {
	return Gallery.find({isActive: true, kind:"Wedding Cake"})
	.then(result => response.send(result));
}

module.exports.getNamePhotos = (request, response) => {
	return Gallery.find({isActive: true, kind:"Name Cake"})
	.then(result => response.send(result));
}

module.exports.getCupCakePhotos = (request, response) => {
	return Gallery.find({isActive: true, kind:"Cake and Cup Cakes"})
	.then(result => response.send(result));
}

module.exports.getChocolatePhotos = (request, response) => {
	return Gallery.find({isActive: true, kind:"Chocolate Cake"})
	.then(result => response.send(result));
}

module.exports.getUniquePhotos = (request, response) => {
	return Gallery.find({isActive: true, kind:"Unique Cake"})
	.then(result => response.send(result));
}

module.exports.getClassicPhotos = (request, response) => {
	return Gallery.find({isActive: true, kind:"Classic Cake"})
	.then(result => response.send(result));
}

module.exports.getThemedPhotos = (request, response) => {
	return Gallery.find({isActive: true, kind:"Themed Cake"})
	.then(result => response.send(result));
}


module.exports.getPhoto = (request, response) => {
	console.log(request.params._id);

	return Gallery.findById(request.params._id)
	.then(result => response.send(result));
}


module.exports.updatePhoto = (request, response) =>{
	const checkAccess = auth.decode(request.headers.authorization).isAdmin;

	if(checkAccess == true){
		let updatePhoto = {
			name: request.body.name,
			kind: request.body.kind,
			image: request.body.image
		}

		return Gallery.findByIdAndUpdate(request.params._id, updatePhoto, {new:true})
		.then(result =>{
			console.log(result);
			response.send(result);
		})
		.catch(error =>{
			console.log(error);
			response.send(false);
		});
	}
	else{
		return response.status(402).send("Admin access required!");
	}
}


module.exports.archivePhoto = (request, response) => {

	const checkAccess = auth.decode(request.headers.authorization).isAdmin;

	let updateIsActiveField = {
		isActive: request.body.isActive
	}

	if(checkAccess == true){
		return Gallery.findByIdAndUpdate(request.params._id, updateIsActiveField)
		.then(result => {
			console.log(result);
			response.send(true);
		})
		.catch(error =>{
			console.log(error);
			response.send(false);
		})
	}
	else{
		return response.status(402).send("Admin access required!");
	}
}

