const express = require("express");
const router = express.Router();
const galleryControllers = require("../controllers/galleryControllers.js")
const auth = require("../auth.js");


router.post("/addPhoto", auth.verify, galleryControllers.addPhoto);

router.get("/allPhotos", galleryControllers.getAllPhotos);

router.get("/messagePhotos", galleryControllers.getMessagePhotos);
router.get("/weddingPhotos", galleryControllers.getWeddingPhotos);
router.get("/namePhotos", galleryControllers.getNamePhotos);
router.get("/cupCakePhotos", galleryControllers.getCupCakePhotos);
router.get("/chocolatePhotos", galleryControllers.getChocolatePhotos);
router.get("/uniquePhotos", galleryControllers.getUniquePhotos);
router.get("/classicPhotos", galleryControllers.getClassicPhotos);
router.get("/themedPhotos", galleryControllers.getThemedPhotos);



router.get("/:_id", galleryControllers.getPhoto);

router.put("/:_id/updatePhoto", auth.verify, galleryControllers.updatePhoto);

router.patch("/:_id/archivePhoto", auth.verify, galleryControllers.archivePhoto);

module.exports = router;

