const express = require("express");
const router = express.Router();
const productControllers = require("../controllers/productControllers.js")
const auth = require("../auth.js");


router.post("/addProduct", auth.verify, productControllers.addProduct);

router.get("/allProducts", productControllers.getAllProducts);

router.get("/activeProducts", productControllers.getActiveProducts);

router.get("/:productId", productControllers.getProduct);

//router.get("/:_id", productControllers.getProductv2);

router.put("/:_id/updateProduct", auth.verify, productControllers.updateProduct);

router.patch("/:_id/archiveProduct", auth.verify, productControllers.archiveProduct);

module.exports = router;