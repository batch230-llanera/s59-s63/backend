const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers.js");
const auth = require("../auth.js");

console.log(userControllers);
router.post("/emailAvailability", userControllers.checkEmailAvailability);

router.post("/register", userControllers.userRegistration);

router.post("/login", userControllers.userLogin);

router.get("/details", auth.verify, userControllers.getProfile);

router.get("/viewUsers", auth.verify, userControllers.getAllUsers);

router.patch("/updateAccess/:_id", auth.verify, userControllers.updateAccess);

router.delete("/deleteUser/:_id", auth.verify, userControllers.deleteUser);


module.exports = router;