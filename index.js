const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const bodyParser = require('body-parser')

const app = express();

const userRoutes = require("./routes/userRoutes.js");
const productRoutes = require("./routes/productRoutes.js");
const orderRoutes = require("./routes/orderRoutes.js");
const galleryRoutes = require("./routes/galleryRoutes.js");

app.use(cors());
app.use(bodyParser.json({limit: "50mb"}));
app.use(bodyParser.urlencoded({limit: "50mb", extended: true, parameterLimit:50000}));

app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/orders", orderRoutes);
app.use("/photos", galleryRoutes);

mongoose.connect("mongodb+srv://admin:admin@batch230.728zfm9.mongodb.net/LeddiesWebpage?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

mongoose.connection.once("open", () => console.log("Now connected to Llanera-Mongo DB Atlas"))

app.listen(process.env.PORT || 4000, () =>
	{ console.log(`API is now online on port ${process.env.PORT || 4000}`)});

